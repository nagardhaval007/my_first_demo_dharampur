#!/usr/bin/env sh
echo 'can pause running builds of CI/CD applications indefinitely. "node server.js"'
echo 'is followed by another command that retrieves the process ID (PID) value'
echo 'of the previously run process (i.e. "node server.js") and writes this value to'
echo 'the file ".pidfile".'
set -x
node server.js
sleep 1
echo $! > .pidfile
set +x

echo 'Now...'
echo 'Visit http://localhost:8081 to see your Node.js/Angular application in action.'
